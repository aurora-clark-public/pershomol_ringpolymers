'''
reformat csv
'''


#####################
## import packages 
#####################
import pandas as pd 
import numpy as np 
import collections
from operator import *


from scipy.spatial.distance import pdist
import dionysus as d 



#####################
## functions 
#####################
def generate_rv_complex(PointsArray, Dimension):
    dists = pdist(PointsArray)
    # adjacentDist = [norm(PointsArray[i,:] - PointsArray[i+1,:]) for i in range(len(PointsArray)-1)]
    Alpha = max(dists)
    rips = d.fill_rips(dists, Dimension, Alpha)
    m = d.homology_persistence(rips)
    dgms = d.init_diagrams(m, rips)
    return rips, m, dgms



#####################
## main
#####################
## load data 
inputPath = ''
df = pd.read_csv(inputPath + "SampleRings.csv")
df = df.dropna()


## generate barcode
targetwindow = df['Snapshot'].unique()
cv_info = []
for targetsnapshot in targetwindow:
    dfTempt = df[df['Snapshot']==targetsnapshot]
    points = np.array(dfTempt.iloc[:,1:4])
    rips,m,dgms = generate_rv_complex(points,3)
    for i,dgm in enumerate(dgms):
        if i<=2:
            for s in dgm:
                if s.death != np.inf:
                    birthGen = list(rips.__getitem__(s.data))
                    deathGen = list(rips.__getitem__(m.pair(s.data)))
                    if set(birthGen).intersection(set(deathGen)) == set(birthGen):
                        flag = 1
                    else:
                        flag = 0
                    cv_info.append([targetsnapshot, i, s.data, s.birth, s.death, birthGen, deathGen, flag])

                else:
                    cv_info.append([targetsnapshot, i, s.data, s.birth, 1.5, list(rips.__getitem__(s.data)), 1, 0])

df_cv_complex = pd.DataFrame(cv_info, columns = ['Snapshot', 'Dimension', 'Filtration', 'Birth', 'Death', 'Generator', 'Terminator', 'Flag'])
df_cv_complex.to_csv(inputPath + "sample_barcode.csv", index = False)





## convert barcode to betti sequence
bettiSeq = np.linspace(0,1.5,601)
bettiSeqDict = collections.defaultdict(list)

snapshots = df_cv_complex['Snapshot'].unique()
for snapshot in snapshots:
        dfSnapshot = df_cv_complex[df_cv_complex['Snapshot']==snapshot]
        bettiSeqDim = []
        for dim in range(2):
            dfSnapshotDim = dfSnapshot[dfSnapshot['Dimension'] == dim]
            dfSnapshotDim = np.array(dfSnapshotDim[['Birth', 'Death']])
            bettiSeqTempt = np.zeros(601)
            for dimlen in range(len(dfSnapshotDim)):
                bettiSeqDimTempt = np.heaviside(bettiSeq - dfSnapshotDim[dimlen,0], 0) - np.heaviside(bettiSeq - dfSnapshotDim[dimlen,1], 0)
                bettiSeqTempt += bettiSeqDimTempt
                bettiSeqTempt[0] = 64
            bettiSeqDim.append(bettiSeqTempt)
        bettiSeqDim = np.array(bettiSeqDim)
        bettiSeqDict[snapshot].append(bettiSeqDim)

np.save(inputPath + 'sample_betti_sequence.npy', bettiSeq)



