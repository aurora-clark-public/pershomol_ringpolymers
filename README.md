# Persistent Homology Computations on Atom Ring Polymers

Python program to study quantum fluctuations and reactive atoms in path integral dynamics, where each atom is represented as a "ring polymer" that captures the uncertainty in its position.

* [Documentation on Wasserstein distance computation](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.wasserstein_distance.html).


### Packages required

* pandas
* numpy
* collections
* pdist (from scipy.spatial.distance)
* [dionysus](https://mrzv.org/software/dionysus2/)  (See [tips](https://passingcuriosity.com/2018/installing-dionysus/) for installing dionysus on OS-X)


The main python program is ***barcode_bettiseq.py***, which works on the included example input file **SampleRings.csv**. This file has 10 snapshots or "rings" of type A atom, each with 64 "beads". The last entry in each row giving the *x,y,z* positions of the atom is ignored by the code.

The code outputs two files: the barcode is output as a csv file (**sample_barcode.csv**) and the Betti sequence is output as an npy file (**sample_betti_sequence.npy**) that could be loaded into Python with the command for further manipulation:

> np.load(inputPath + 'sample_betti_sequence.npy', allow_pickle=True)

The **inputpath** is set to be empty by default (but could be altered inside the main code file as desired by the user).


## Sample Data of the Kob-Anderson Glass Forming System

A sample subset of types A and B atoms with 64 beads each is now available (files **A_64rep.xyz** and **B_64rep.xyz**). One could run the entire analysis pipeline on this data for testing and learning purposes.